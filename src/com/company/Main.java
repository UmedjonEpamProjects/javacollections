package com.company;

import com.Text.CollectionService;
import com.Text.CollectionServiceImpl;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        CollectionService collectionService = new CollectionServiceImpl();

        System.out.println("Method №1 ");
        System.out.println("Repeated Words: \n" + collectionService.RepeatWord());
        System.out.println("Method №2");
        System.out.println("Unique Words: \n" + collectionService.UniqueWord());

    }
}
