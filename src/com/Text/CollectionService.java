package com.Text;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface CollectionService {

    String file() throws IOException;

    String recognize(String s);

    Map<String, Long> RepeatWord() throws IOException;

    List<String> UniqueWord();

}
