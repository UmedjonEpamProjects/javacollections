package com.Text;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CollectionServiceImpl implements CollectionService {


    @Override
    public String file() throws IOException {
        File file = new File("C:\\Users\\Umedjon\\IdeaProjects\\Collections\\text.txt");
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        String tx = "";
        do {
            if (!((line = bufferedReader.readLine()) != null)) {
                break;
            }
            tx = tx.concat(line);
        } while (true);
        bufferedReader.close();
        fileReader.close();
        return tx;
    }

    @Override
    public String recognize(String s) {
        s = s.toLowerCase().replaceAll("[^A-Za-zА-Яа-я]\\s+", " ");
        return s;
    }

    @Override
    public Map<String, Long> RepeatWord() throws IOException {
        Map<String, Long> map = new HashMap<>();
        String[] split = recognize(file()).split("\\s");
        for (int i = 0; i < split.length; i++) {
            String s = split[i];
            map.put(s, map.containsKey(s) ? map.get(s) + 1 : 1);
        }
        return map;

    }

    @Override
    public List<String> UniqueWord() {

        List<String> entryList = new ArrayList<>();
        try {
            for (Map.Entry entry : RepeatWord().entrySet()) {
                String key = (String) entry.getKey();
                Long value = (Long) entry.getValue();
                if (value == 1) {
                    entryList.add(key);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return entryList;
    }

}
